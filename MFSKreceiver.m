%
% Based on FMreceiver.m created by AJ
%

clear all;
clc;

% set the sample rate (65.1kSPS - 61.44MSPS)
fs = 200e3;

% set the center frequency of the spectrum analyzer
fcenter=1e9;

% set hamming constants
n = 15;
k = 11;

% Initialize SDR
sdrdev('Pluto');

% Initialize SDR Receiver Functionality
radio_rx = sdrrx('Pluto',...
    'RadioID',              'usb:0',...
    'CenterFrequency',      fcenter,...
    'BasebandSampleRate',   fs,...
    'GainSource',           'AGC Fast Attack',...
    'OutputDataType',       'double',...
    'SamplesPerFrame',      20000);


% Create a spectrum analyzer scope to visualize the signal spectrum
scope = dsp.SpectrumAnalyzer(...
    'Name',                 'Spectrum Analyzer',...
    'Title',                'Spectrum Analyzer', ...
    'SpectrumType',         'Power',...
    'FrequencySpan',        'Full', ...
    'SampleRate',           fs, ...
    'YLimits',              [-60,40],...
    'SpectralAverages',     10, ...
    'FrequencySpan',        'Start and stop frequencies', ...
    'StartFrequency',       -fs/2, ...
    'StopFrequency',        fs/2, ...
    'Position',             figposition([20 10 60 60]));


% Do an initial RX
data = radio_rx();

mfskDemod = comm.FSKDemodulator('ModulationOrder',16,'BitOutput',true,'SymbolMapping','Binary',...
    'FrequencySeperation',10000,'SymbolRate',100,'OutputDataType','logical');

% Receive
disp('Starting Now');
data  = radio_rx();

% De-modulate and error-correct:

demodulated = mfskDemod(data);

errorCorrected = decode(demodulated,n,k,'hamming/binary');
errorCorrected = reshape(errorCorrected, length(S)/8, 8);
output = char(bin2dec(errorCorrected));

% Display spectrum and data:
scope(data)

disp(output)

% Release Pluto resources
release(radio_rx);
disp('Done');