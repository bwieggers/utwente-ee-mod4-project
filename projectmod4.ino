#include <Arduino.h>
#include <vector>
#include <driver/dac.h>
#include <WiFi.h>
#include <bitset>
using namespace std;

// Constants for wifi AP
const char* ssid     = "best transmit system ever";
const char* password = "ajiscool";
const char* hostname = "transmitter.local";

const int parityCount = 4; // Amount of parity bits excluding the 0th bit. We are using Hamming(15/11)

// Set web server port number to 80
WiFiServer server(80);

// Variable to store the HTTP request
String header;

void insertHamming(vector<bool>& stream){
  // Define constants
  const int blockL = pow(2,parityCount)-1;
  const int messageL = blockL-parityCount;

  // Make stream nicely divisable
  int tail = messageL - (stream.size() % messageL);
  for(int i=0; i < tail; i++){
    stream.push_back(0);
  }

  // Loop through blocks and insert codes
  for(int i=0; i < stream.size(); i+=(blockL+1)){
    bool block[blockL+1] = {};
    bool parityBits[parityCount] = {};
    bool overallParity = 0;

    // Fill block with data
    int j = 0;
    for(int h=1; h <= blockL; h++){ // Counting from h because the if-statement doesn't work for 0
      if((h & (h - 1)) == 0){ // If position h is a parity bit (power of 2), keep it at 0 and skip
        continue;
      }
      block[h] = stream[i+j];
      j++;
    }

    // Calculate parities

    for(int h=1; h <= blockL; h++){ // Calculate j=h=0 afterwards
      for(int k=0; k <= parityCount; k++){
        if(h & (1 << k)){
          parityBits[k] ^= block[h];
        }
      } 
      overallParity ^= block[h];
    }
    // overallParity is now parity excluding parity bits. Include them:
    for(bool h : parityBits){
      overallParity ^= h;
    }

    // Insert parities into datastream

    stream.insert(stream.begin()+i, overallParity);
    for(int h=0; h < parityCount; h++){
      stream.insert(stream.begin()+i+pow(2,h), parityBits[h]);;
    }
  }

}


void sendmessage(String& message) {
  // Passing by reference is better than by value but not good enough; 
  //    a stream directly piped from the wifi connection would be better.
  //    Due to time contraints, we have chosen to use this simple fix that
  //    is good enough for the demo.

  // Link voltages to be put on the varicap to the bitpairs
  int noOfFrequencies = 16;
  double voltages[noOfFrequencies+1]; // [000, 001, 010, 011, ... , 111, base]
  voltages[0] = 1; // Lowest frequency
  voltages[noOfFrequencies] = 2; // Base frequency
  double increment = 0.1; // Voltage increment between frequencies
  double maxVoltage = 3.3;

  //Generate voltage array
  for(int i = 0; i < noOfFrequencies; i++){
    voltages[i+1] = voltages[i]+increment;
  }

  // Rewrite data and incorporate error correction

  //    Convert to binary vector

  vector<bool> stream; 
  // A vector<bool> is used instead of a bitset because of the dynamic size
  // This is the bottleneck of the algorithm. It has to copy everything :(
  for( char i : message ){
    bitset<8> dataset = i;
    for(int j = 0; j < 8; j++){
      stream.push_back(dataset[j]);
    }
  }

  insertHamming(stream);

  // Send calibration waves
  dac_output_enable(DAC_CHANNEL_1);
  dac_output_voltage(DAC_CHANNEL_1, voltages[noOfFrequencies]/maxVoltage * 255);
  delay(1000);

  for(double i : voltages){
    dac_output_voltage(DAC_CHANNEL_1, i/maxVoltage * 255);
    delay(100);
  }


  // Send data
  for(int i = 0; i <= stream.size(); i += 4){
    int freq = 0;
    for(int j = 0; j < 4; j++){
      freq = freq + (stream[i+j] << j);
    }
    dac_output_voltage(DAC_CHANNEL_1, voltages[freq]/maxVoltage * 255);
    delay(10); // Should be as short as possible. Tune by trial-and-error. Not sure how fast and reliable the ESP and oscillator can switch.
  }

  // Shutdown
  dac_output_voltage(DAC_CHANNEL_1, 0);


}

void setup() {
  // Setup wifi
  Serial.begin(115200);
  WiFi.softAP(ssid, password);
  WiFi.setHostname(hostname);
  Serial.println(WiFi.softAPIP());
  server.begin();

  
    
}

void loop() {
  // Serve webpage
  // If there is an input, send it to sendmessage()
  WiFiClient client = server.available();
  if (client){
    Serial.println("New Client.");
    String currentLine = "";
    while (client.connected()){
      if (client.available()){
        char c = client.read();
        header += c;
        if (c == '\n') {
          if (currentLine.length() == 0){
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();

            client.println("<!DOCTYPE HTML><html><body>");
            client.println("<form action=\"/get\">Send text: <input type=\"text\" name=\"textinput\"><input type=\"submit\" value=\"Send\"></form>");
            //client.println("<form action=\"/get\">Send file: <input type=\"file\" name=\"fileinput\"><input type=\"submit\" value=\"Send\"></form>");
            // Due to time constraints, we did not manage to get file upload working
            client.println("</body></html>");
            client.println();

            if( header.startsWith("GET /get?textinput=") ) {
              // This takes the HTTP encoded string. Due to time constraints this is good enough.
              String message = header.substring(19, header.length()-11);
              Serial.println("Sending message:" + message);
              sendmessage(message);
            }
            break;

          }
        }
      }
    }
    header = "";
    client.stop();
    Serial.println("Client disconnected");
    Serial.println("");
  }

  

}
